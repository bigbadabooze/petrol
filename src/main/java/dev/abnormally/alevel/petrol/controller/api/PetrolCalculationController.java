package dev.abnormally.alevel.petrol.controller.api;

import dev.abnormally.alevel.petrol.entity.Petrol;
import dev.abnormally.alevel.petrol.repository.PetrolRepository;
import dev.abnormally.alevel.petrol.request.PetrolCalculationRequest;
import dev.abnormally.alevel.petrol.request.petrol.PetrolCalculationDTO;
import dev.abnormally.alevel.petrol.request.petrol.PetrolResponsePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/petrol")
@PropertySource(value = "classpath:constant.properties", ignoreResourceNotFound = true)
public class PetrolCalculationController {
    private PetrolRepository repository;

    @Value(value = "${petrol.price:2.}")
    private Double constantPrice;

    @Value(value = "${petrol.consumption:10}")
    private Integer constantConsumption;

    // This one can be fully constant. Don't think there is gonna be breaking changes in metrics system.
    @Value(value = "${miles.to.km:1.60934}")
    private Double constantMilesToKilometers;

    @PostMapping("")
    public PetrolResponsePair calculate(@Valid @RequestBody PetrolCalculationRequest request) {
        List<PetrolCalculationDTO> assets = request.getAssets();

        if (assets.size() > 0) {
            Petrol petrol = calculatePetrol(assets.get(0));
            PetrolResponsePair petrolPair = new PetrolResponsePair(petrol, petrol);

            repository.saveAll(
                    assets
                            .stream()
                            .map(petrolDTO -> petrolPair.checkMinMax(calculatePetrol(petrolDTO)))
                            .collect(Collectors.toList())
            );

            return petrolPair;
        }

        return new PetrolResponsePair(null, null);
    }

    /**
     * Helper functions
     */

    private Double countPetrolTotalPaymentAmount(Integer route) {
        return constantPrice * ((route * constantMilesToKilometers) / 100 * constantConsumption);
    }

    private Petrol calculatePetrol(PetrolCalculationDTO petrolDTO) {
        return petrolDTO.toPetrol().setTotalPetrolSum(
                countPetrolTotalPaymentAmount(petrolDTO.toPetrol().getRoute())
        );
    }

    @Autowired
    public void setRepository(PetrolRepository repository) {
        this.repository = repository;
    }

}
