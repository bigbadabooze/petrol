package dev.abnormally.alevel.petrol.entity;

import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
public class Petrol {
    @Id
    @ReadOnlyProperty
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, updatable = false)
    private Long userId;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Integer route;

    @Column(nullable = false)
    private Double totalPetrolSum;

    private LocalDateTime createdAt;

    public Petrol() { /**/ }

    public Long getId() {
        return id;
    }

    @NotNull
    public Long getUserId() {
        return userId;
    }

    @NotNull
    public String getName() {
        return name;
    }


    @NotNull
    public Integer getRoute() {
        return route;
    }

    public Double getTotalPetrolSum() {
        return totalPetrolSum;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    @PrePersist
    public void setCreatedAt() {
        this.createdAt = LocalDateTime.now();
    }

    public Petrol setUserId(Long userId) {
        this.userId = userId;

        return this;
    }

    public Petrol setName(String name) {
        this.name = name;

        return this;
    }

    public Petrol setRoute(Integer route) {
        this.route = route;

        return this;
    }

    public Petrol setTotalPetrolSum(Double totalPetrolSum) {
        this.totalPetrolSum = totalPetrolSum;

        return this;
    }

    @Override
    public String toString() {
        return String.format("Petrol { id = %d, userId = %d, name = %s, route = %d, createdAt = %s }",
                getId(),
                getUserId(),
                getName(),
                getRoute(),
                getCreatedAt()
        );
    }

}
