package dev.abnormally.alevel.petrol.request.petrol;

import dev.abnormally.alevel.petrol.entity.Petrol;

public class PetrolResponsePair implements PetrolResponsePairContract {
    private Petrol minimal;
    private Petrol maximal;

    public PetrolResponsePair(Petrol minimal, Petrol maximal) {
        this.minimal = minimal;
        this.maximal = maximal;
    }

    @Override
    public Petrol getMinimal() {
        return minimal;
    }

    @Override
    public Petrol getMaximal() {
        return maximal;
    }

    @Override
    public void setMinimal(Petrol minimal) {
        this.minimal = minimal;
    }

    @Override
    public void setMaximal(Petrol maximal) {
        this.maximal = maximal;
    }

    @Override
    public void checkMinimal(Petrol other) {
        if (minimal.getTotalPetrolSum() < other.getTotalPetrolSum())
            minimal = other;
    }

    @Override
    public void checkMaximal(Petrol other) {
        if (maximal.getTotalPetrolSum() > other.getTotalPetrolSum())
            maximal = other;
    }

}
