package dev.abnormally.alevel.petrol.request.petrol;

import dev.abnormally.alevel.petrol.entity.Petrol;
import dev.abnormally.utils.Pair;

public interface PetrolResponsePairContract extends Pair<Petrol, Petrol> {

    Petrol getMaximal();

    Petrol getMinimal();

    void setMinimal(Petrol minimal);

    void setMaximal(Petrol maximal);

    void checkMinimal(Petrol other);

    void checkMaximal(Petrol other);

    default Petrol checkMinMax(Petrol other) {
        checkMinimal(other);
        checkMaximal(other);

        return other;
    }

}
