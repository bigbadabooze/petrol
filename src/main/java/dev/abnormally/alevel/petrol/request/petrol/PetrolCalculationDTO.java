package dev.abnormally.alevel.petrol.request.petrol;

import dev.abnormally.alevel.petrol.entity.Petrol;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class PetrolCalculationDTO {
    private Long id;
    private String name;
    private Integer route;
    private Petrol petrol;

    @NotNull
    @Min(1)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    @Length(min = 3, max = 512)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    public Integer getRoute() {
        return route;
    }

    public void setRoute(Integer route) {
        this.route = route;
    }

    public Petrol toPetrol() {
        if (Objects.isNull(petrol)) {
            petrol = new Petrol().setUserId(getId()).setName(getName()).setRoute(getRoute());
        }

        return petrol;
    }

}
