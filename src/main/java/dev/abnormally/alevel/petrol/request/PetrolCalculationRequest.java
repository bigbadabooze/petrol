package dev.abnormally.alevel.petrol.request;

import dev.abnormally.alevel.petrol.request.petrol.PetrolCalculationDTO;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class PetrolCalculationRequest {
    private Long id;
    private List<PetrolCalculationDTO> assets;

    public PetrolCalculationRequest() { /**/ }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Valid
    @NotNull
    public List<PetrolCalculationDTO> getAssets() {
        return assets;
    }

    public void setAssets(ArrayList<PetrolCalculationDTO> assets) {
        this.assets = assets;
    }

    @Override
    public String toString() {
        return String.format("PetrolCalculationRequest %s", getAssets());
    }

}
