package dev.abnormally.alevel.petrol.repository;

import dev.abnormally.alevel.petrol.entity.Petrol;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetrolRepository extends JpaRepository<Petrol, Long> { /**/ }
