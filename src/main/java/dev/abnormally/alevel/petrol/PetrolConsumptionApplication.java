package dev.abnormally.alevel.petrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetrolConsumptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetrolConsumptionApplication.class, args);
	}

}
