package dev.abnormally.utils;

import java.io.Serializable;

public interface Pair<First, Second> extends Serializable { /**/ }
